# Web Sockets

MtGox also provides a streaming API here - <https://socketio.mtgox.com/mtgox>. The API can be accessed using HTML5 Websockets, but is most easily accessed using [socket.io](http://socket.io). Some very complete documentation is available on the bitcoin wiki, here <https://en.bitcoin.it/wiki/MtGox/API/Streaming>.

If you want to try out the API, or  connect to any WebSocket server, try ws.client.html in this directory. Simply enter the server url and click connect. You can then send a JSON message by pasting the JSON code (remember, strings must be enclosed with double quotes!). Note that MtGox's API sends updates very rapidly, so you may want to turn autoscrolling off to look at individual messages.

If the client doesn't seem to work, make sure you are using a modern browser, I recommend Safari, Chrome or Firefox. I made and tested it around Safari, but any of those browsers should work well.

Feel free to look at or play around with the code. The client is really just to test using websockets, but it may give some helpful pointers to implementing a websocket client yourself.

*If you find this client useful, please consider donating: **1GfwKioWJ6qjfYGH37wmgjRifF2DaqT4U6** :)*