# Examples

This directory will hopefully grow to contain examples of using the API in a variety of languages. Feel free to send in any source code you have constructed :) A list of the current implementations is below:

Language | Author | Description
--- | --- | ---
Python | nitrous | A quick and basic class I made for use in my simple MtGox ticker example (<https://bitbucket.org/nitrous/simple-mtgox-ticker>).
VB.Net | nitrous | An implementation I made on request to demonstrate a working example.
node.js | ameen3 | Sent in by Ameen, the main project is here (<https://github.com/ameen3/node-mtgox-apiv2>). Dependencies are querystring and jsSHA.

