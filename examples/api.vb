Module GoxAPI

	Sub Main()
		' Example API implementation in VBA
		' Not complete
		' VBA is hard to implement the API in, would recommend against
		' Still need to decode the JSON response

		Dim goxKey, goxSec, goxSign, basePath, nonce, method, postData, response As String
		' MtGox API Specifics
		Dim version As Integer : version = 2
		basePath = "https://data.mtgox.com/api/" & version.ToString() & "/"
		nonce = Math.Round(DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds).ToString() & "000"

		' Fill these in
		goxKey = "Your API Key"
		goxSec = "Your API Secret"

		' The API method
		method = "BTCUSD/money/ticker"
		postData = "nonce=" & nonce

		' Compute HMAC
		goxSign = HmacSHA512(method & Chr(0) & postData, goxSec)

		' Make and send Request
		Dim request As System.Net.HttpWebRequest
		Dim stream As System.IO.Stream
		Dim wresponse As System.Net.HttpWebResponse
		Dim asc As Object : asc = New System.Text.UTF8Encoding()
		Dim data() As Byte : data = asc.GetBytes(postData)
		Dim path As String : path = basePath & method
		request = System.Net.WebRequest.Create(path)

		Console.WriteLine("Path: " & path)
		Console.WriteLine("Data: " & postData)
		Console.WriteLine(data.Length & "/" & postData.Length)

		With request
			.Method = "POST"
			.ContentLength = data.Length
			.UserAgent = "btc_bot"
			.Headers.Add("Rest-key", goxKey)
			.Headers.Add("Rest-sign", goxSign)

			stream = .GetRequestStream()
			stream.Write(data, 0, data.Length)
			stream.Close()

			Try
				wresponse = .GetResponse()
			Catch ex As System.Net.WebException
				wresponse = ex.Response
				Console.WriteLine("HTTP Code: " & wresponse.StatusCode)
			End Try

			stream = wresponse.GetResponseStream()
			Dim reader As New System.IO.StreamReader(stream)
			response = reader.ReadToEnd()

			reader.Close()
			stream.Close()
			wresponse.Close()
		End With

		' MsgBox(response)
		Console.WriteLine("Response: " & response)
		Console.ReadKey()

		' Check that the request was successful
		' Now, response contains a JSON string
		' It needs to be decoded
		' You can check the 'result' key for 'success'
		' Otherwise it may be 'error'
	End Sub

	Public Function HmacSHA512(ByVal msg As String, ByVal secret As String)
		Dim asc, enc As Object
		Dim bytes() As Byte
		Dim bMsg() As Byte

		asc = New System.Text.UTF8Encoding()
		enc = New System.Security.Cryptography.HMACSHA512()

		bMsg = asc.GetBytes(msg)
		enc.Key = System.Convert.FromBase64String(secret)

		bytes = enc.ComputeHash(bMsg)
		HmacSHA512 = System.Convert.ToBase64String(bytes)

		asc = Nothing
		enc = Nothing
	End Function

End Module